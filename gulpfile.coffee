require('coffee-script/register')
gulp = require 'gulp'
watch = require 'gulp-watch'
coffee = require 'gulp-coffee'
gutil = require 'gulp-util'
coffeelint = require 'gulp-coffeelint'
browserSync = require 'browser-sync'

gulp.task 'browser-sync', ->
    browserSync.init null,
    open: true
    server:
      baseDir: "./dist"
    watchOptions:
      debounceDelay: 1000


gulp.task 'coffee', ->
  gulp.src 'coffee/*.coffee'
    .pipe coffeelint()
    .pipe coffee({bare: true}).on 'error', gutil.log
    .pipe gulp.dest('dist/js')

gulp.task 'html', ->
  gulp.src('*.html')
  .pipe(gulp.dest('dist'))

gulp.task 'css', ->
  gulp.src('css/*.css')
  .pipe(gulp.dest('dist/css'))

gulp.task 'lib', ->
  gulp.src('lib/**')
  .pipe(gulp.dest('dist/js'))

gulp.task 'watch', ->
  gulp.watch('coffee/*.coffee',['coffee', browserSync.reload])
  gulp.watch('css/*.css',['css', browserSync.reload])
  gulp.watch('*.html',['html', browserSync.reload])

  gulp.watch ['./dist'], (file) ->
    browserSync.reload(file.path) if file.type is "changed"
    #gulp.watch().on 'change', livereload.changed;

gulp.task 'default', ['coffee','html','lib','css','browser-sync','watch']