paper = Snap "#svg"
#Plugin to send the element to back or to front
###############
Snap.plugin (Snap, Element, Paper, glob) ->
    elproto = Element.prototype;
    elproto.toFront = -> @prependTo(this.paper)
    elproto.toBack = -> @appendTo(this.paper)
###############

#Ellipse constants
a = 200	 
b = 30 
x0 = 200 
y0 = 200
r = 40

#Animation controlers
anim = true 


#Electrons count
count = document.getElementById('count').value;
if count % 2 is 0 then count /= 2 

#Define ATOMs array 
atoms = new Array
t = 0


#Hmmm, class of electron
class Electron
	constructor:(index) ->
		angle = index*360/count
		@color = true
		@paint= paper
				.circle x0, y0, 10
				.attr
					fill: "#6AAED2"
		@ellipse= paper
				.ellipse(x0, y0, a, b)
				.attr
					fill: "transparent"
					stroke: "#35465A"
					strokeWidth: 3
					transform: "r-#{angle}"
				.toFront()
		this.animateElectron index
		this.animateElectronColor index
	animateElectron:(index)->
		me = this
		@paint.animate {
		"cx" : x0+(a)*Math.cos(t)*Math.cos(index*Math.PI*(360/count)/180) - (b)*Math.sin(t)*Math.sin(index*Math.PI*(360/count)/180), 
		"cy": y0+(a)*Math.cos(t)*Math.sin(index*Math.PI*(360/count)/180) + (b)*Math.sin(t)*Math.cos(index*Math.PI*(360/count)/180)},
		1, 
		mina.linear, -> 
			t = if t < Math.PI*2 then t+0.09/count else 0
			me.animateElectron index
	animateElectronColor:(index)->
		me = this
		@paint.animate {
		"fill": if me.color then '#35465A' else '#6AAED2'},
		1000, 
		mina.linear, -> 
			me.color = not me.color
			me.animateElectronColor index





#Creation of electrons
for i in [0..count-1] by 1
	atoms[i] = new Electron i+1


#####Cell style + init#####
style = 
	fill: '#676673'
	stroke: '#35465A'
	strokeWidth: 5
cell = paper
		.circle x0, y0, r
		.attr style

#Bouncing animation to cell
setInterval (-> cell.animate {
							"r": if anim then 40 else 45 
							"fill": if anim then "#6AAED2" else "#35465A"}, 300, mina.bounce, -> anim = !anim),300
