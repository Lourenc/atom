# Atom Demo with Snap.SVG
Demo that draws atom with animated electrons.


### Version
0.0.1



### Development
Project uses Gulp and CoffeeScript.
Gulpfile written in Coffee. 

Run this :
```sh
$ gulp
```
### Author
Korney Vasilchenko

### Todo's

 - Make classes for electrons and atom
 - Save this projext to GitHub
License
----

MIT