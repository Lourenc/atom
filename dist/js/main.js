var Electron, a, anim, atoms, b, cell, count, i, paper, r, style, t, x0, y0, _i, _ref;

paper = Snap("#svg");

Snap.plugin(function(Snap, Element, Paper, glob) {
  var elproto;
  elproto = Element.prototype;
  elproto.toFront = function() {
    return this.prependTo(this.paper);
  };
  return elproto.toBack = function() {
    return this.appendTo(this.paper);
  };
});

a = 200;

b = 30;

x0 = 200;

y0 = 200;

r = 40;

anim = true;

count = document.getElementById('count').value;

if (count % 2 === 0) {
  count /= 2;
}

atoms = new Array;

t = 0;

Electron = (function() {
  function Electron(index) {
    var angle;
    angle = index * 360 / count;
    this.color = true;
    this.paint = paper.circle(x0, y0, 10).attr({
      fill: "#6AAED2"
    });
    this.ellipse = paper.ellipse(x0, y0, a, b).attr({
      fill: "transparent",
      stroke: "#35465A",
      strokeWidth: 3,
      transform: "r-" + angle
    }).toFront();
    this.animateElectron(index);
    this.animateElectronColor(index);
  }

  Electron.prototype.animateElectron = function(index) {
    var me;
    me = this;
    return this.paint.animate({
      "cx": x0 + a * Math.cos(t) * Math.cos(index * Math.PI * (360 / count) / 180) - b * Math.sin(t) * Math.sin(index * Math.PI * (360 / count) / 180),
      "cy": y0 + a * Math.cos(t) * Math.sin(index * Math.PI * (360 / count) / 180) + b * Math.sin(t) * Math.cos(index * Math.PI * (360 / count) / 180)
    }, 1, mina.linear, function() {
      t = t < Math.PI * 2 ? t + 0.09 / count : 0;
      return me.animateElectron(index);
    });
  };

  Electron.prototype.animateElectronColor = function(index) {
    var me;
    me = this;
    return this.paint.animate({
      "fill": me.color ? '#35465A' : '#6AAED2'
    }, 1000, mina.linear, function() {
      me.color = !me.color;
      return me.animateElectronColor(index);
    });
  };

  return Electron;

})();

for (i = _i = 0, _ref = count - 1; _i <= _ref; i = _i += 1) {
  atoms[i] = new Electron(i + 1);
}

style = {
  fill: '#676673',
  stroke: '#35465A',
  strokeWidth: 5
};

cell = paper.circle(x0, y0, r).attr(style);

setInterval((function() {
  return cell.animate({
    "r": anim ? 40 : 45,
    "fill": anim ? "#6AAED2" : "#35465A"
  }, 300, mina.bounce, function() {
    return anim = !anim;
  });
}), 300);
